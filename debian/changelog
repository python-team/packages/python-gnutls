python-gnutls (3.1.2-2) UNRELEASED; urgency=medium

  * Use debhelper-compat instead of debian/compat.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Ondřej Nový <onovy@debian.org>  Sat, 20 Jul 2019 00:09:08 +0200

python-gnutls (3.1.2-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * [524cf08] d/control: Set Vcs-* to salsa.debian.org
  * [2ac9eb7] d/copyright: Use https protocol in Format field
  * [ef2b23a] d/changelog: Remove trailing whitespaces
  * [bfc77a6] Convert git repository from git-dpm to gbp layout

  [ Bernd Zeimetz ]
  * [92b84ce] Add debian/.gitlab-ci.yml
  * [c4869d6] Update upstream source from tag 'upstream/3.1.2'
    Update to upstream version '3.1.2'
    with Debian dir c1e8923d8826994317c502c6903963990fd44d70

 -- Bernd Zeimetz <bzed@debian.org>  Sat, 02 Feb 2019 17:29:02 +0100

python-gnutls (3.0.0-1) unstable; urgency=medium

  * Svn to git migration.
  [ Stefano Rivera ]
  * Move inline patch to a 3.0 (quilt) patch, in preparation for maintaining
    package with git-dpm.

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ Orestis Ioannou ]
  * New upstream release.
  * Remove patch since it was merged upstream.
  * Switched build system to pybuild.
  * Changed copyright to DEP-5 format.
  * Modified format to indicate 3.0 quilt.
  * Pump up s-d version to 3.9.8, No changes needed.
  * Move debhelper compat to v9. No changes needed.
  * Wrap-and-sort'ed.

 -- Orestis Ioannou <orestis@oioannou.com>  Tue, 26 Apr 2016 23:13:23 +0200

python-gnutls (2.0.1-2) unstable; urgency=medium

  * Team upload.
  * Use a function that actually exists to test if OpenPGP functions are
    present in the library (Closes: #764034)
    - Thanks to Teddy Hogeborn for the bug report, the diagnosis, and
      the initial version of the patch
    - Note: Change is done inline to avoid adding a patch system during
      freeze, so it does not appear in the DPMT svn

 -- Scott Kitterman <scott@kitterman.com>  Sun, 23 Nov 2014 20:34:20 -0500

python-gnutls (2.0.1-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Depend and build-depend on gnutls28 (Closes: #736525)

 -- Jonathan Wiltshire <jmw@debian.org>  Sat, 04 Oct 2014 10:50:49 +0100

python-gnutls (1.2.5-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Bernd Zeimetz ]
  * Updating to upstream version 1.2.5.

 -- Bernd Zeimetz <bzed@debian.org>  Thu, 13 Feb 2014 23:51:02 +0100

python-gnutls (1.2.4-1) unstable; urgency=low

  * New upstream version.
  * Migrate to dh_python2.

 -- Bernd Zeimetz <bzed@debian.org>  Thu, 26 Apr 2012 21:39:12 +0200

python-gnutls (1.2.0-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix "gnutls.library.functions fails with libgnutls28-3.0.8-2":
    apply patch taken from upstream 1.2.4 release. (Closes: #651865)

 -- gregor herrmann <gregoa@debian.org>  Sat, 03 Mar 2012 15:05:46 +0100

python-gnutls (1.2.0-2) unstable; urgency=low

  * Depend on libgnutls26 as the library is used via ctypes now.
    A better solution will be implemented after Squeeze.

 -- Bernd Zeimetz <bzed@debian.org>  Wed, 06 Oct 2010 16:53:00 +0200

python-gnutls (1.2.0-1) unstable; urgency=low

  * New upstream version.

 -- Bernd Zeimetz <bzed@debian.org>  Tue, 02 Feb 2010 21:14:38 +0100

python-gnutls (1.1.9-2) unstable; urgency=low

  [ Luca Falavigna ]
  * debian/control:
    - Build-depend on python-all-dev (>= 2.5).
    - Add XS-Python-Version field.
    - Drop python-ctypes from Depends (Closes: #562481).

  [ Bernd Zeimetz ]
  * Bumping Standards-Version to 3.8.3, no changes needed.

 -- Bernd Zeimetz <bzed@debian.org>  Sun, 10 Jan 2010 15:25:49 +0100

python-gnutls (1.1.9-1) unstable; urgency=low

  * New upstream release.
  * Bumping Standards-Version to 3.8.2, no changes needed.

 -- Bernd Zeimetz <bzed@debian.org>  Wed, 15 Jul 2009 22:17:08 +0200

python-gnutls (1.1.8-1) unstable; urgency=low

  [ Bernd Zeimetz ]
  * New upstream version
  * debian/control:
    - Rising libgnutls build dependency to (>= 2.4.1)
    - Bumping Standards-Version, no changes needed.

  [ Sandro Tosi ]
  * debian/control
    - switch Vcs-Browser field to viewsvn

 -- Bernd Zeimetz <bzed@debian.org>  Sun, 15 Feb 2009 23:28:46 +0100

python-gnutls (1.1.6-1) unstable; urgency=low

  * New upstream version.

 -- Bernd Zeimetz <bzed@debian.org>  Thu, 24 Jul 2008 01:59:04 +0200

python-gnutls (1.1.5-2) unstable; urgency=low

  * debian/control:
    - The minimal version of libgnutls-dev needs to be (>= 2.2.2).

 -- Bernd Zeimetz <bzed@debian.org>  Thu, 08 May 2008 09:43:33 +0200

python-gnutls (1.1.5-1) unstable; urgency=low

  * New upstream release.

  [ Bernd Zeimetz ]
  * debian/control:
    - Adding Homepage field, removing pseudo-field from description
    - Rename XS-Vcs-* fields to Vcs-* (dpkg supports them now)
    - Updating my email address
    - Bumping Standards-Version to 3.7.3, no changes needed.
    - Remove -1 revision from python-all-dev Build-Dep.
    - Fix minor spelling error in the description.

  [ Sandro Tosi ]
  * debian/watch
    - url simplification for new pypi
  * debian/control
    - uniforming Vcs-Browser field

 -- Bernd Zeimetz <bzed@debian.org>  Wed, 07 May 2008 18:09:02 +0200

python-gnutls (1.1.4-1) unstable; urgency=low

  * New upstream version
  * debian/control:
    - Adding XS-Vcs-Svn and XS-Vcs-Browser fields

 -- Bernd Zeimetz <bernd@bzed.de>  Thu, 20 Sep 2007 19:34:37 +0200

python-gnutls (1.1.3-1) unstable; urgency=low

  [ Bernd Zeimetz ]
  * New upstream version

  [ Piotr Ożarowski ]
  * Provide pythonX.Y-gnutls packages

 -- Bernd Zeimetz <bernd@bzed.de>  Sat, 25 Aug 2007 19:52:30 +0200

python-gnutls (1.1.2-1) unstable; urgency=low

  * Initial release (Closes: #428866)

 -- Bernd Zeimetz <bernd@bzed.de>  Thu, 14 Jun 2007 20:15:56 +0100
